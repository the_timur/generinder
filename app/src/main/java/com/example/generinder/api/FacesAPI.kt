package com.example.generinder.api

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

interface FacesAPI {
    @GET("random-image-url")
    fun getRandomUrl(): UrlData

    @GET("random-image")
    fun getRandomImage(): Call<ResponseBody>
}
