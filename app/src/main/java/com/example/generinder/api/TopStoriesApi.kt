package com.example.generinder.api

import retrofit2.http.GET

interface TopStoriesApi : Api {
    @GET("v0/topstories.json")
    override suspend fun getList(): List<Int>
}
