package com.example.generinder.api

import retrofit2.http.GET

interface NewStoriesApi : Api {
    @GET("v0/newstories.json")
    override suspend fun getList(): List<Int>
}
