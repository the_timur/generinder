package com.example.generinder.api

import retrofit2.http.GET

interface BestStoriesApi : Api {
    @GET("v0/beststories.json")
    override suspend fun getList(): List<Int>
}
