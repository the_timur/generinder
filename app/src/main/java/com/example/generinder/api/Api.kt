package com.example.generinder.api

import com.example.generinder.ui.topcoder.Story
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {
    suspend fun getList(): List<Int>

    @GET("v0/item/{story_id}.json")
    suspend fun getStory(@Path(value = "story_id", encoded = true) story_id: Int): Story
}
