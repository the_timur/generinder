package com.example.generinder.ui.newstories

import com.example.generinder.api.NewStoriesApi
import com.example.generinder.ui.topcoder.StoriesViewModel
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NewStoriesViewModel : StoriesViewModel() {

    override fun provideApi(): NewStoriesApi {
        return Retrofit.Builder()
            .baseUrl("https://hacker-news.firebaseio.com/")
            .client(provideOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NewStoriesApi::class.java)
    }
}
