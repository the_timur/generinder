package com.example.generinder.ui.home

import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.generinder.R
import com.example.generinder.database.AppDatabase
import com.example.generinder.database.rates.ImageRate
import com.example.generinder.database.rates.ImageRatesRepository
import com.example.generinder.databinding.FragmentHomeBinding
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream
import java.util.UUID

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private val dao by lazy { ImageRatesRepository(AppDatabase.getDatabase(context!!).imageRatesDao()) }
    private var currentBitmap: Bitmap? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val loading = binding.progressIndicator
        val image = binding.imageView
        val textView: TextView = binding.textHome
        homeViewModel.text.observe(viewLifecycleOwner) {
            textView.text = it
        }
        val upButton = binding.rateUp
        val downButton = binding.rateBad

        upButton.setOnClickListener {
            image.isVisible = false
            val file = saveBitmap()
            val rate = ImageRate(0, "url", 1, file.absolutePath)
            viewLifecycleOwner.lifecycleScope.launch {
                dao.insert(rate)
            }
            homeViewModel.reloadImage()
            image.isVisible = true
        }
        downButton.setOnClickListener {
            image.isVisible = false
            val file = saveBitmap()
            val rate = ImageRate(0, "url", 0, file.absolutePath)
            viewLifecycleOwner.lifecycleScope.launch {
                dao.insert(rate)
            }
            homeViewModel.reloadImage()
            image.isVisible = true
        }

        homeViewModel.imageState.observe(viewLifecycleOwner) {
            when (it) {
                is HomeViewModel.ViewState.Loading -> {
                    loading.isVisible = true
                }
                is HomeViewModel.ViewState.Data -> {
                    val bitmap = it.bitmap
                    currentBitmap = bitmap
                    if (bitmap == null) {
                        textView.text = getString(R.string.loadError)
                    } else {
                        image.setImageBitmap(it.bitmap)
                        loading.isVisible = false
                    }
                }
            }
        }

        return root
    }

    private fun saveBitmap(): File {
        val file = File(context?.getExternalFilesDir(Environment.DIRECTORY_PICTURES), UUID.randomUUID().toString())
        val outputStream = FileOutputStream(file)
        currentBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
        outputStream.close()
        return file
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
