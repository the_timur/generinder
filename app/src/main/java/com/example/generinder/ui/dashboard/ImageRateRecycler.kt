package com.example.generinder.ui.dashboard

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.generinder.R
import com.example.generinder.database.rates.ImageRate
import java.io.File

class ImageRateRecycler : RecyclerView.Adapter<ImageRateRecycler.ViewHolder>() {
    // <3
    var top: MutableList<ImageRate> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_rate, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val imageRate = top[position]

        if (imageRate.image == null) {
            holder.avatarImageView.setImageResource(R.drawable.ic_launcher_foreground)
        } else {
            val file = File(imageRate.image)
            val bitmap = BitmapFactory.decodeFile(file.absolutePath)
            holder.avatarImageView.setImageBitmap(bitmap)
        }
        holder.storyUrl.text = "Image $position"
        holder.scoreGrade.text = "Score: ${if (imageRate.rate > 0) {"Good"} else {"Bad"} }"

//        holder.avatarImageView.setOnClickListener {
//            val builder = AlertDialog.Builder(it.rootView.context)
//            val dialogView = LayoutInflater.from(it.rootView.context).inflate(R.layout.custom_dialog, null)
//
//            val dialogAvatar = dialogView.findViewById<ImageView>(R.id.dialog_pic)!!
//            val dialogAuthor = dialogView.findViewById<TextView>(R.id.dialog_author)!!
//            val dialogTitle = dialogView.findViewById<TextView>(R.id.dialog_title)!!
//            val dialogScore = dialogView.findViewById<TextView>(R.id.dialog_score)!!
//            val dialogCommentCount = dialogView.findViewById<TextView>(R.id.dialog_comments)!!
//            val dialogButton = dialogView.findViewById<Button>(R.id.dialog_button)!!
//
//            dialogAvatar.setImageResource(R.mipmap.ic_launcher)
//            dialogTitle.text = imageRate.title
//            dialogScore.text = "${imageRate.score}"
//            dialogAuthor.text = "story by " + imageRate.by
//            updateCommentCount(holder.commentTextView, dialogCommentCount, imageRate)
//            dialogButton.setOnClickListener {
//                updateCommentCount(holder.commentTextView, dialogCommentCount, imageRate)
//                val intentBrowser = Intent(Intent.ACTION_VIEW, Uri.parse("https://news.ycombinator.com/item?id=${imageRate.id}"))
//
//                intentBrowser.flags = Intent.FLAG_ACTIVITY_NEW_TASK;
//                mContext.startActivity(intentBrowser)
//            }
//
//            builder.setView(dialogView)
//            builder.setCancelable(true)
//            builder.show()
//        }
    }

    override fun getItemCount(): Int {
        return top.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val avatarImageView = itemView.findViewById<ImageView>(R.id.avatarImageView)!!
        val storyUrl = itemView.findViewById<TextView>(R.id.imageUrl)!!
        val scoreGrade = itemView.findViewById<TextView>(R.id.imageRate)!!
    }
}
