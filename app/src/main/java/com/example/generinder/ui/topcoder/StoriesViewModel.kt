package com.example.generinder.ui.topcoder

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.generinder.api.Api
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

open class StoriesViewModel : ViewModel() {
    private val _viewState = MutableLiveData<ViewState>(ViewState.Loading).apply {
        viewModelScope.launch {
            value = ViewState.Data(getTopArticles())
        }.start()
    }

    open val viewState: LiveData<ViewState> = _viewState

    sealed class ViewState {
        object Loading : ViewState()
        data class Data(val userList: List<Story>) : ViewState()
    }

    private suspend fun getTopArticles(): List<Story> {
        return withContext(Dispatchers.IO) {
            try {
                val provider = provideApi()
                val topList = provider.getList()
                topList.take(20).map { provider.getStory(it) }
            } catch (e: Exception) {
                println("No connection")
                listOf()
            }
        }
    }

    open fun provideApi(): Api {
        return Retrofit.Builder()
            .baseUrl("https://hacker-news.firebaseio.com/")
            .client(provideOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(Api::class.java)
    }

    open fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(5, TimeUnit.SECONDS)
            .retryOnConnectionFailure(false)
            .build()
    }

    fun reloadList() {
        _viewState.apply {
            value = ViewState.Loading

            viewModelScope.launch {
                value = ViewState.Data(getTopArticles())
            }.start()
        }
    }
}
