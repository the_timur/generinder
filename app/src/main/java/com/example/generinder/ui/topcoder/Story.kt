package com.example.generinder.ui.topcoder

data class Story(
    val id: Long,
    val by: String,
    val descendants: Int?,
    val score: Int,
    val time: Int,
    val title: String,
    val url: String?,
)
