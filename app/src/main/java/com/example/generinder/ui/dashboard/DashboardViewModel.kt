package com.example.generinder.ui.dashboard

import androidx.lifecycle.*
import com.example.generinder.database.rates.ImageRate
import com.example.generinder.database.rates.ImageRatesRepository
import kotlinx.coroutines.launch

class DashboardViewModel(private val repository: ImageRatesRepository) : ViewModel() {
    val listState: LiveData<List<ImageRate>> = repository.allRates.asLiveData()

    fun insert(rate: ImageRate) = viewModelScope.launch {
        repository.insert(rate)
    }

    private val _text = MutableLiveData<String>().apply {
        value = "This is dashboard Fragment"
    }
    val text: LiveData<String> = _text
}

class DashboardViewModelFactory(private val repository: ImageRatesRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DashboardViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return DashboardViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
