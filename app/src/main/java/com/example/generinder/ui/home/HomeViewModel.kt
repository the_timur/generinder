package com.example.generinder.ui.home

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.generinder.api.FacesAPI
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class HomeViewModel : ViewModel() {

    private val _imageState = MutableLiveData<ViewState>(ViewState.Loading).apply {
        viewModelScope.launch {
            value = ViewState.Data(loadImage())
        }.start()
    }
    val imageState: LiveData<ViewState> = _imageState

    sealed class ViewState {
        object Loading : ViewState()
        data class Data(val bitmap: Bitmap?) : ViewState()
    }

    private val _text = MutableLiveData<String>().apply {
        value = "Rate this image"
    }
    val text: LiveData<String> = _text

    private fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(5, TimeUnit.SECONDS)
            .retryOnConnectionFailure(false)
            .build()
    }

    private fun provideFacesApi(): FacesAPI {
        return Retrofit.Builder()
            .baseUrl("https://100k-faces.glitch.me/")
            .client(provideOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(FacesAPI::class.java)
    }

    private suspend fun loadImage(): Bitmap? {
        return withContext(Dispatchers.IO) {
            try {
                val provider = provideFacesApi()
                val call = provider.getRandomImage()
                BitmapFactory.decodeStream(call.execute().body()?.byteStream())
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }
    }

    fun reloadImage() {
        _imageState.apply {
            value = ViewState.Loading

            viewModelScope.launch {
                value = ViewState.Data(loadImage())
            }.start()
        }
    }
}
