package com.example.generinder.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.generinder.database.AppDatabase
import com.example.generinder.database.rates.ImageRatesRepository
import com.example.generinder.databinding.FragmentDashboardBinding

class DashboardFragment : Fragment() {

    private var _binding: FragmentDashboardBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val dao = AppDatabase.getDatabase(context!!).imageRatesDao()
        val dashboardViewModel =
            ViewModelProvider(this, DashboardViewModelFactory(ImageRatesRepository(dao))).get(DashboardViewModel::class.java)

        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.usersRecyclerView.adapter = ImageRateRecycler()
//
//        val textView: TextView = binding.textDashboard
//        dashboardViewModel.text.observe(viewLifecycleOwner) {
//            textView.text = it
//        }

        dashboardViewModel.listState.observe(viewLifecycleOwner) {
            (binding.usersRecyclerView.adapter as ImageRateRecycler).apply {
                top = it.toMutableList()
                notifyDataSetChanged()
            }
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
