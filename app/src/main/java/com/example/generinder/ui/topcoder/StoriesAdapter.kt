package com.example.generinder.ui.topcoder

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.generinder.R

class StoriesAdapter(var mContext: Context) : RecyclerView.Adapter<StoriesAdapter.ViewHolder>() {
    var top: List<Story> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val story = top[position]

        holder.avatarImageView.setImageResource(R.mipmap.ic_launcher)
        holder.storyTitleTextView.text = story.title
        holder.scoreTextView.text = "Score: ${story.score}"

        updateCommentCount(holder.commentTextView, null, story)

        holder.avatarImageView.setOnClickListener {
            val builder = AlertDialog.Builder(it.rootView.context)
            val dialogView = LayoutInflater.from(it.rootView.context).inflate(R.layout.custom_dialog, null)

            val dialogAvatar = dialogView.findViewById<ImageView>(R.id.dialog_pic)!!
            val dialogAuthor = dialogView.findViewById<TextView>(R.id.dialog_author)!!
            val dialogTitle = dialogView.findViewById<TextView>(R.id.dialog_title)!!
            val dialogScore = dialogView.findViewById<TextView>(R.id.dialog_score)!!
            val dialogCommentCount = dialogView.findViewById<TextView>(R.id.dialog_comments)!!
            val dialogButton = dialogView.findViewById<Button>(R.id.dialog_button)!!

            dialogAvatar.setImageResource(R.mipmap.ic_launcher)
            dialogTitle.text = story.title
            dialogScore.text = "${story.score}"
            dialogAuthor.text = "story by " + story.by
            updateCommentCount(holder.commentTextView, dialogCommentCount, story)
            dialogButton.setOnClickListener {
                updateCommentCount(holder.commentTextView, dialogCommentCount, story)
                val intentBrowser = Intent(Intent.ACTION_VIEW, Uri.parse("https://news.ycombinator.com/item?id=${story.id}"))

                intentBrowser.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                mContext.startActivity(intentBrowser)
            }

            builder.setView(dialogView)
            builder.setCancelable(true)
            builder.show()
        }
    }

    private fun updateCommentCount(
        dialogCommentCount: TextView?,
        viewCommentCount: TextView?,
        article: Story,
    ) {
        viewCommentCount?.text = "Commented ${article.descendants ?: 0} times"
        dialogCommentCount?.text = "${article.descendants ?: 0} comments"
    }

    override fun getItemCount(): Int {
        return top.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val avatarImageView = itemView.findViewById<ImageView>(R.id.avatarImageView)!!
        val storyTitleTextView = itemView.findViewById<TextView>(R.id.storyTitleTextView)!!
        val scoreTextView = itemView.findViewById<TextView>(R.id.scoreTextView)!!
        val commentTextView = itemView.findViewById<TextView>(R.id.commentCountTextView)!!
    }
}
