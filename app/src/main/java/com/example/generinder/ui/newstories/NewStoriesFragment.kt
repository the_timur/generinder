package com.example.generinder.ui.newstories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.generinder.databinding.FragmentNewStoriesBinding
import com.example.generinder.ui.topcoder.StoriesAdapter
import com.example.generinder.ui.topcoder.StoriesViewModel

class NewStoriesFragment : Fragment() {

    private var _binding: FragmentNewStoriesBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val notificationsViewModel =
            ViewModelProvider(this).get(NewStoriesViewModel::class.java)

        _binding = FragmentNewStoriesBinding.inflate(inflater, container, false)
        binding.usersRecyclerView.adapter = StoriesAdapter(context!!)

        binding.container.setOnRefreshListener {
            binding.container.isRefreshing = false
            notificationsViewModel.reloadList()
        }

        notificationsViewModel.viewState.observe(viewLifecycleOwner) {
            when (it) {
                is StoriesViewModel.ViewState.Loading -> {
                    binding.progressIndicator.isVisible = true
                    binding.usersRecyclerView.isVisible = false
                    binding.errorText.isVisible = false
                }
                is StoriesViewModel.ViewState.Data -> {
                    binding.usersRecyclerView.isVisible = true
                    binding.errorText.isVisible = it.userList.isEmpty()
                    (binding.usersRecyclerView.adapter as StoriesAdapter).apply {
                        top = it.userList
                        notifyDataSetChanged() // diff util for single element reload
                    }
                    binding.progressIndicator.isVisible = false
                }
            }
        }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
