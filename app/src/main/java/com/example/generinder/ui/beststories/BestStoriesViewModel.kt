package com.example.generinder.ui.beststories

import com.example.generinder.api.BestStoriesApi
import com.example.generinder.ui.topcoder.StoriesViewModel
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class BestStoriesViewModel : StoriesViewModel() {

    override fun provideApi(): BestStoriesApi {
        return Retrofit.Builder()
            .baseUrl("https://hacker-news.firebaseio.com/")
            .client(provideOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(BestStoriesApi::class.java)
    }
}
