package com.example.generinder.ui.topstories

import com.example.generinder.api.TopStoriesApi
import com.example.generinder.ui.topcoder.StoriesViewModel
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class TopStoriesViewModel : StoriesViewModel() {
    override fun provideApi(): TopStoriesApi {
        return Retrofit.Builder()
            .baseUrl("https://hacker-news.firebaseio.com/")
            .client(provideOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(TopStoriesApi::class.java)
    }
}
