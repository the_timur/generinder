package com.example.generinder.database.rates

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface ImageRateDao {
    @Insert
    suspend fun insert(vararg rates: ImageRate)

    @Delete
    suspend fun delete(vararg rates: ImageRate)

    @Query("SELECT * FROM image_rates")
    fun getAll(): Flow<List<ImageRate>>
}
