package com.example.generinder.database.rates

import kotlinx.coroutines.flow.Flow

class ImageRatesRepository(private val ratesData: ImageRateDao) {

    val allRates: Flow<List<ImageRate>> = ratesData.getAll()

    suspend fun insert(rate: ImageRate) {
        ratesData.insert(rate)
    }
}
