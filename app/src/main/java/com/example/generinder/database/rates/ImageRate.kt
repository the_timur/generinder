package com.example.generinder.database.rates

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "image_rates")
data class ImageRate(
    @PrimaryKey(autoGenerate = true) val id: Int,

    @ColumnInfo(name = "url") val url: String,
    @ColumnInfo(name = "rate") val rate: Int = 0,
    @ColumnInfo(name = "image") val image: String? = null,
)
